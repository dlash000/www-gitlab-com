---
layout: markdown_page
title: "Conference Booth Setup"
---

## Summary

This page focuses on how to set up two of the ways which demo assets are shown at conference booths.

## Main screen

Play the "GitLab Overview for Booths" video in a loop from a laptop.
- [File](https://drive.google.com/file/d/1tZCWxwTZcAmrURdAfhKA5VCXvItxJHln/view?usp=sharing)
- [YouTube](https://youtu.be/ttbU90SJbfk)

This video is silent and shows a combination of slides and product video clips in an end to end use case format.

## iPads

### Accounts
- US ipads
  - Pin: 0000
  - AppleID login: gitlabnconf (password in Marketing 1Password vault)
- Google login: gitlabconf@gmail.com (password in Marketing 1Password vault)

### Load required software
1. Open App Store (login with above Apple ID)
1. Install Google Drive
1. Install Google Slides

### Load required files

If you are reading this page on the ipad you are setting up then here is the [click-through drive folder](https://drive.google.com/drive/u/0/folders/1Qm8Y3oVLRa0nS1BARA631Ex6SKVzYp3C)

1. Open Google Drive
1. login with above Google login
1. Find the actual files:
   1. If you are reading this page on the ipad you are setting up then here is the [click-through drive folder](https://drive.google.com/drive/u/0/folders/1Qm8Y3oVLRa0nS1BARA631Ex6SKVzYp3C)  
   **OR** 
   1. Go to "Shared with me" menu item or look at the "Shared" view (on the bottom) and find the "Click-through" folder that's been shared and open it
1. To the right of each file are "...". Click on that and select "Make available off-line" for each file you want to have access to
1. For each file, star them (this'll make easier to find later)

If you are reading this page on the ipad you are setting up then here is the [click-through drive folder](https://drive.google.com/drive/u/0/folders/1Qm8Y3oVLRa0nS1BARA631Ex6SKVzYp3C)

### Using at booth
1. To open, open Google slides
1. This should show your files, select which one to view
1. In off-line mode, presentation mode doesn't work, but you can show the slides still
1. It can be effective to use iPad pinch/un-pinch to zoom in/out on parts of demo

